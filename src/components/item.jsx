import React, { Component } from 'react'
import ItemList from "./item-list"

class Item extends Component {
    constructor(props) {
        super(props);
        this.state = {
            item: [],
            container: '',
        }
    }
    updateListHandler = () => {
        let date = new Date();
        let id = date.getTime();
        let addNewData = { id: id, message: this.state.container }
        let [...data] = this.state.item;
        data.push(addNewData);
        this.setState({
            item: data
        })
    }
    handleRemove = (id) => {
        let [...data] = this.state.item;
        let newData = data.filter(content => content.id !== id);
        this.setState({
            item: newData
        })

    }
    inputFormChange = (event) => {
        let data = event.target.value
        this.setState({
            container: data
        })
    }
    render() {
        return (
            <div>
                <h1>Todo List </h1>
                <input type="text" onChange={this.inputFormChange} />
                <button className="btn btn-outline-success" onClick={this.updateListHandler}>Update List</button>
                <ItemList data={this.state.item} remove={this.handleRemove} />
            </div>
        )
    }
}

export default Item;