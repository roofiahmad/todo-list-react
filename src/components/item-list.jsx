import React, { Component } from 'react'
import Content from './content'
import './itemlist.css'

class ItemList extends Component {

    render() {
        return (
            <div className="item-list">
                <ul> {this.props.data.map((item) =>
                    <li key={item.id}>
                        <Content item={item} remove={this.props.remove} />
                    </li>)}
                </ul>
            </div>
        )
    }

}

export default ItemList;