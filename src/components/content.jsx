import React, { Component } from 'react'
import './content.css'

class Content extends Component {
    constructor(props) {
        super(props);
        this.state = {
            checked: false
        }
    }
    handleCheck = (event) => {
        let status = event;
        this.setState({
            checked: status
        })
    }

    render() {
        return (
            <div className="content btn btn-outline-success" onClick={() => this.handleCheck(!this.state.checked)}>
                <input type="checkbox" onChange={(event) => this.handleCheck(event.target.checked)} />
                <p className={this.state.checked ? 'checked' : 'unchecked'}  >{this.props.item.message}</p>
                <button className="remove btn btn-primary" onClick={() => { this.props.remove(this.props.item.id) }}>remove</button>
            </div>
        )
    }
}

export default Content;