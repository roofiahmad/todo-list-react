import React, { useState } from 'react'

const Loggin = () => {
    const [isLoggedIn, setLogged] = useState(true)
    const handleClick = (e) => {
        console.log('click works')
        setLogged(!isLoggedIn);
    }

    return (
        <div>
            <h1 >Is Logged {isLoggedIn ? "in" : "out"}</h1>
            <p>{isLoggedIn ? "Selamat Datang" : "Terimakasih sudah datang"}</p>
            <button onClick={(e) => handleClick(e)}> Click Here</button>
        </div>
    )
}

export default Loggin